﻿using BasicAuthentication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace WebApplication1.Controllers
{

    //[RoutePrefix("dds")]
    //[IdentityBasicAuthentication] 
    //[Authorize] // Require some form of authentication
    public class DefaultController : ApiController
    {
        //默认 GET: api/Default
        //[Route("")]
        public IHttpActionResult Get()
        {
            HomeModel model = new HomeModel
            {
                UserName = User.Identity.Name
            };

            ClaimsIdentity identity = User.Identity as ClaimsIdentity;

            if (identity != null)
            {
                List<ClaimModel> claims = new List<ClaimModel>();

                foreach (Claim claim in identity.Claims)
                {
                    claims.Add(new ClaimModel
                    {
                        Type = claim.Type,
                        Value = claim.Value
                    });
                }

                model.Claims = claims;
            }

            return Json(model);
        }

        // GET: api/Default/5
        //[Route("{id}")]
        public IHttpActionResult Get(int id)
        {
            HomeModel model = new HomeModel
            {
                UserName = User.Identity.Name
            };

            ClaimsIdentity identity = User.Identity as ClaimsIdentity;

            if (identity != null)
            {
                List<ClaimModel> claims = new List<ClaimModel>();

                foreach (Claim claim in identity.Claims)
                {
                    claims.Add(new ClaimModel
                    {
                        Type = claim.Type,
                        Value = claim.Value
                    });
                }

                model.Claims = claims;
            }

            return Json(model);
        }

        // POST: api/Default
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Default/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Default/5
        public void Delete(int id)
        {
        }
    }
}
