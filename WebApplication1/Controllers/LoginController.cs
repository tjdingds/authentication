﻿using Authorization.Helpers;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Mime;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Results;

namespace WebApplication1.Controllers
{
    [AllowAnonymous]
    public class LoginController : ApiController
    {
        [HttpGet]
        public IHttpActionResult getToken()
        {
            var urls = new List<PermissionItem>();
            urls.Add(new PermissionItem { Role="", Url="/api/default" });

            var payload = new Dictionary<string, object>
            {
                { ClaimTypes.Name, "SampleUser" },
                { "exp", DateTimeOffset.UtcNow.AddMinutes(5).ToUnixTimeSeconds()},
                { ClaimTypes.Role,""},
                { ClaimTypes.Uri,urls }
            };

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm(); // symmetric
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            string tokenSecret = ConfigurationManager.AppSettings["tokenSecret"] ?? "";
            var token = encoder.Encode(payload, tokenSecret);

            return Json(token);
        }
    }
}
