﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AuthorizationBasic.Data;
using System.Security.Claims;
using JWT.Algorithms;
using JWT.Serializers;
using JWT;
using System.Configuration;

namespace AuthorizationBearer
{
    //// Enable authentication via an ASP.NET Identity user name and password
    
    public class JwtBearerAuthenticationAttribute : BearerAuthenticationAttribute
    {
        readonly string tokenSecret = ConfigurationManager.AppSettings["tokenSecret"] ?? "";
        protected override async Task<IPrincipal> AuthenticateAsync(IDictionary<string,object> claims, CancellationToken cancellationToken)
        {
            
            cancellationToken.ThrowIfCancellationRequested();// Unfortunately, UserManager doesn't support CancellationTokens.

            ClaimsIdentity identity = new ClaimsIdentity(new List<Claim> { new Claim(ClaimTypes.Name, claims[ClaimTypes.Name].ToString()), new Claim(ClaimTypes.Expiration, claims[ClaimTypes.Expiration].ToString()) },"Bearer");
            
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);
            //create a ClaimsIdentity with all the Claims for this user
            cancellationToken.ThrowIfCancellationRequested();

            return await Task.FromResult(principal);
       }

        private static UserManager<IdentityUser> CreateUserManager()
        {
            
            return new UserManager<IdentityUser>(new UserStore<IdentityUser>(new UsersDbContext()));
        }
    }
}