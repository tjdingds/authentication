﻿using Authorization;
using Authorization.Helpers;
using JWT;
using JWT.Algorithms;
using JWT.Exceptions;
using JWT.Serializers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Script.Serialization;

namespace AuthorizationBearer
{
    public abstract class BearerAuthenticationAttribute : Attribute, IAuthenticationFilter
    {

        
        public string Realm { get; set; }
        public virtual bool AllowMultiple => false;

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            HttpRequestMessage request = context.Request;
            AuthenticationHeaderValue authentication = request.Headers.Authorization;

            if (authentication == null)
            {
                return;
            }
            if (authentication.Scheme != "Bearer")
            {
                return;
            }
            if (string.IsNullOrEmpty(authentication.Parameter))
            {
                context.ErrorResult = new AuthenticationFailureResult("Missing credentials", request);
                return;
            }

            Tuple<IDictionary<string,object>,string> decodeToken = DecodeToken(authentication.Parameter);

            IDictionary<string, object> claims = decodeToken.Item1;
            if (claims == null || claims.Count == 0)
            {
                context.ErrorResult = new AuthenticationFailureResult(decodeToken.Item2, request);
                return;
            }

            
            var urls = new List<PermissionItem>();
            if (!claims.ContainsKey(ClaimTypes.Uri))
            {
                context.ErrorResult = new AuthenticationFailureResult("token no include Uri ", request);
                return;
            }
            else
            {
                urls = new JavaScriptSerializer().Deserialize<List<PermissionItem>>(claims[ClaimTypes.Uri].ToString());
                if (urls.Where(x => x.Url == "/api/"+request.GetRouteData().Values["controller"].ToString()).Count() <= 0)
                {
                    context.ErrorResult = new AuthenticationFailureResult("url denty ", request);
                    return;
                }
            }

            var principal = await AuthenticateAsync(claims, cancellationToken);

            if (principal == null)
            {
                // Authentication was attempted but failed. Set ErrorResult to indicate an error.
                context.ErrorResult = new AuthenticationFailureResult("Invalid token", request);
            }
            else
            {
                // Authentication was attempted and succeeded. Set Principal to the authenticated user.
                context.Principal = principal;
                
            }

        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            Chanllenge(context);
            return Task.FromResult(0);
        }

        private void Chanllenge(HttpAuthenticationChallengeContext context)
        {
            context.ChallengeWith("Bearer");
        }

        protected abstract Task<IPrincipal> AuthenticateAsync(IDictionary<string, object> claims,CancellationToken cancellationToken); 

        private static  Tuple<IDictionary<string, object>, string> DecodeToken(string token)
        {
            string tokenSecret = ConfigurationManager.AppSettings["tokenSecret"] ?? "";
            IDictionary<string, object> payload = new Dictionary<string,object>();
            string errormsg = "";
            try
            {
                IJsonSerializer serializer = new JsonNetSerializer();
                var provider = new UtcDateTimeProvider();
                IJwtValidator validator = new JwtValidator(serializer, provider);
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtAlgorithm algorithm = new HMACSHA256Algorithm(); // symmetric
                IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder, algorithm);

                payload = decoder.DecodeToObject<IDictionary<string, object>>(token,tokenSecret,true);
            }
            catch (TokenExpiredException)
            {
                errormsg = "Token has expired";
            }
            catch (SignatureVerificationException)
            {
                errormsg = "Token has invalid signature";
            }
            return new Tuple<IDictionary<string, object>, string>(payload, errormsg);
        }
    }
}