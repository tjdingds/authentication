﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using AuthorizationBasic.Data;
using System.Security.Claims;

namespace AuthorizationBasic
{
    //// Enable authentication via an ASP.NET Identity user name and password
    
    public class IdentityBasicAuthenticationAttribute : BasicAuthenticationAttribute
    {
        protected override async Task<IPrincipal> AuthenticateAsync(string userName, string password, CancellationToken cancellationToken)
        {
            UserManager<IdentityUser> userManager = CreateUserManager();
            cancellationToken.ThrowIfCancellationRequested();// Unfortunately, UserManager doesn't support CancellationTokens.
            IdentityUser user = await userManager.FindAsync(userName, password);
            if (user == null)
            {
                return null;
            }
            //create a ClaimsIdentity with all the Claims for this user
            cancellationToken.ThrowIfCancellationRequested();
            ClaimsIdentity identity = await userManager.ClaimsIdentityFactory.CreateAsync(userManager, user, "Basic");
            return new ClaimsPrincipal(identity);
        }

        private static UserManager<IdentityUser> CreateUserManager()
        {
            
            return new UserManager<IdentityUser>(new UserStore<IdentityUser>(new UsersDbContext()));
        }
    }
}