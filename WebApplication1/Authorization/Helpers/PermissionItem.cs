﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Authorization.Helpers
{
    public class PermissionItem
    {
        /// <summary>
        /// 用户或者角色或者其他凭据实体
        /// </summary>
        public virtual string Role { get; set; } = "";

        /// <summary>
        /// 请求url
        /// </summary>
        public virtual string Url { get; set; } = "";
    }
}