﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Authorization
{
    public class AuthenticationFailureResult : IHttpActionResult
    {
        public string ReasionPhrase { get; private set; }
        public HttpRequestMessage Request { get; private set; }
        public AuthenticationFailureResult(string resionPhrase, HttpRequestMessage request)
        {
            ReasionPhrase = resionPhrase;
            Request = request;
        }
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        public HttpResponseMessage Execute()
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            response.RequestMessage = Request;
            response.ReasonPhrase = ReasionPhrase;
            return response;
        }
    }
}