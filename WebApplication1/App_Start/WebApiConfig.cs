﻿using AuthorizationBasic;
using AuthorizationBearer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WebApplication1
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API 配置和服务  
            //config.Filters.Add(new IdentityBasicAuthenticationAttribute());//Basic
            config.Filters.Add(new JwtBearerAuthenticationAttribute());//Basic
            config.Filters.Add(new AuthorizeAttribute());
            // Web API 路由
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { controller ="Default" ,id = RouteParameter.Optional }
            );
        }
    }
}
